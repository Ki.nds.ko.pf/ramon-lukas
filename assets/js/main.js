/*==================== MENU SHOW Y HIDDEN ====================*/
const navMenu = document.getElementById('nav-menu'),
    navToggle = document.getElementById('nav-toggle'),
    navClose = document.getElementById('nav-close')


/*===== MENU SHOW =====*/
/* Validate if constant exists */
if (navToggle) {
    navToggle.addEventListener('click', () => {
        navMenu.classList.add('show-menu')
    })
}

/*===== MENU HIDDEN =====*/
/* Validate if constant exists */
if (navClose) {
    navClose.addEventListener('click', () => {
        navMenu.classList.remove('show-menu')
    })
}

/*==================== REMOVE MENU MOBILE ====================*/
const navLink = document.querySelectorAll('.nav__link')

function linkAction() {
    const navMenu = document.getElementById('nav-menu')
    // When we click on each nav__link, we remove the show-menu class
    navMenu.classList.remove('show-menu')
}
navLink.forEach(n => n.addEventListener('click', linkAction))

/*==================== ACCORDION SKILLS ====================*/
const skillsContent = document.getElementsByClassName('skills__content'),
    skillsHeader = document.querySelectorAll('.skills__header')

function toggleSkills() {
    let itemClass = this.parentNode.className

    for (i = 0; i < skillsContent.length; i++) {
        skillsContent[i].className = 'skills__content skills__close'
    }
    if (itemClass === 'skills__content skills__close') {
        this.parentNode.className = 'skills__content skills__open'
    }
}

skillsHeader.forEach((el) => {
    el.addEventListener('click', toggleSkills)
})

/*==================== QUALIFICATION TABS ====================*/
const tabs = document.querySelectorAll('[data-target]'),
    tabContents = document.querySelectorAll('[data-content]')

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        const target = document.querySelector(tab.dataset.target)

        tabContents.forEach(tabContent => {
            tabContent.classList.remove('qualification__active')
        })
        target.classList.add('qualification__active')

        tabs.forEach(tab => {
            tab.classList.remove('qualification__active')
        })
        tab.classList.add('qualification__active')
    })
})

/*==================== SERVICES MODAL ====================*/
const modalViews = document.querySelectorAll('.services__modal'),
    modalBtns = document.querySelectorAll('.services__button'),
    modalCloses = document.querySelectorAll('.services__modal-close')

let modal = function (modalClick) {
    modalViews[modalClick].classList.add('active-modal')
}

modalBtns.forEach((modalBtn, i) => {
    modalBtn.addEventListener('click', () => {
        modal(i)
    })
})

modalCloses.forEach((modalClose) => {
    modalClose.addEventListener('click', () => {
        modalViews.forEach((modalView) => {
            modalView.classList.remove('active-modal')
        })
    })
})

/*==================== PORTFOLIO SWIPER  ====================*/
let swiperPortfolio = new Swiper('.portfolio__container', {
    cssMode: true,
    loop: true,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
})

/*==================== TESTIMONIAL ====================*/
let swiperTestimonial = new Swiper('.testimonial__container', {
    loop: true,
    grabCursor: true,
    spaceBetween: 48,

    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        dynamicBullets: true,
    },
    breakpoints: {
        568: {
            slidesPerView: 2,
        },
    }
})


/*==================== SCROLL SECTIONS ACTIVE LINK ====================*/
const sections = document.querySelectorAll('section[id]')

function scrollActive() {
    const scrollY = window.pageYOffset

    sections.forEach(current => {
        const sectionHeight = current.offsetHeight
        const sectionTop = current.offsetTop - 50;
        sectionId = current.getAttribute('id')

        if (scrollY > sectionTop && scrollY <= sectionTop + sectionHeight) {
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.add('active-link')
        } else {
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.remove('active-link')
        }
    })
}
window.addEventListener('scroll', scrollActive)

/*==================== CHANGE BACKGROUND HEADER ====================*/
function scrollHeader() {
    const nav = document.getElementById('header');
    if (this.scrollY >= 80) nav.classList.add('scroll-header'); else nav.classList.remove('scroll-header');
}
window.addEventListener('scroll', scrollHeader)

/*==================== SHOW SCROLL TOP ====================*/
function scrollTop() {
    let scrollTop = document.getElementById('scroll-top');
    // When the scroll is higher than 560 viewport height, add the show-scroll class to the a tag with the scroll-top class
    if (this.scrollY >= 200) scrollTop.classList.add('show-scroll'); else scrollTop.classList.remove('show-scroll')
}
window.addEventListener('scroll', scrollTop)

/*==================== DARK LIGHT THEME ====================*/
const themeButton = document.getElementById('theme-button')
const darkTheme = 'dark-theme'
const iconTheme = 'uil-sun'

// Previously selected topic (if user selected)
const selectedTheme = localStorage.getItem('selected-theme')
const selectedIcon = localStorage.getItem('selected-icon')

// We obtain the current theme that the interface has by validating the dark-theme class
const getCurrentTheme = () => document.body.classList.contains(darkTheme) ? 'dark' : 'light'
const getCurrentIcon = () => themeButton.classList.contains(iconTheme) ? 'uil-moon' : 'uil-sun'

// We validate if the user previously chose a topic
if (selectedTheme) {
    // If the validation is fulfilled, we ask what the issue was to know if we activated or deactivated the dark
    document.body.classList[selectedTheme === 'dark' ? 'add' : 'remove'](darkTheme)
    themeButton.classList[selectedIcon === 'uil-moon' ? 'add' : 'remove'](iconTheme)
}

// Activate / deactivate the theme manually with the button
themeButton.addEventListener('click', () => {
    // Add or remove the dark / icon theme
    document.body.classList.toggle(darkTheme)
    themeButton.classList.toggle(iconTheme)
    // We save the theme and the current icon that the user chose
    localStorage.setItem('selected-theme', getCurrentTheme())
    localStorage.setItem('selected-icon', getCurrentIcon())
})

// color change
const purple = document.getElementById('purple-button')

purple.addEventListener('click', () => {
    // Add or remove the dark / icon theme
    document.body.classList.toggle("purple-theme")
    themeButton.classList.toggle(iconTheme)
    // We save the theme and the current icon that the user chose

})

function getElement(id) {
    return document.getElementById(id);
}




fetch('../../assets/data/data.json').then(response => {
    return response.json();
}).then(data => {
    /* document.getElementById("demo").innerHTML = data; */
    /* console.log(data); */

    setText(data.header_id, data.header_name)

    setText(data.tabs.home.tab_title_id, data.tabs.home.tab_title)
    setText(data.tabs.home.title_id, data.tabs.home.title)
    setText(data.tabs.home.subtitle_id, data.tabs.home.subtitle)
    setText(data.tabs.home.text_id, data.tabs.home.text)
    setText(data.tabs.home.button_id, data.tabs.home.button)

    setText(data.scrolling_id, data.scrolling)

    /* about */
    const about = data.tabs.about
    setText(about.tab_title_id, about.tab_title)
    setText(about.title_id, about.tab_title)
    setText(about.subtitle_id, about.subtitle)
    setText(about.text_id, about.text)
    setText(about.button_id, about.button)
    /* about info */
    setText(about.infos.years.title_id, about.infos.years.title)
    setText(about.infos.years.name_id, about.infos.years.name)
    setText(about.infos.projects.title_id, about.infos.projects.title)
    setText(about.infos.projects.name_id, about.infos.projects.name)
    setText(about.infos.companies.title_id, about.infos.companies.title)
    setText(about.infos.companies.name_id, about.infos.companies.name)

    /* skills */
    setText(data.tabs.skills.tab_title_id, data.tabs.skills.tab_title)
    setText(data.tabs.skills.title_id, data.tabs.skills.tab_title)
    setText(data.tabs.skills.subtitle_id, data.tabs.skills.subtitle)
    /* skills content */
    const frontend = data.tabs.skills.content.frontend
    setText(frontend.title_id, frontend.title)
    setText(frontend.subtitle_id, frontend.subtitle)
    setText(frontend.list.html.name_id, frontend.list.html.name)
    setText(frontend.list.html.number_id, frontend.list.html.number)
    setText(frontend.list.css.name_id, frontend.list.css.name)
    setText(frontend.list.css.number_id, frontend.list.css.number)
    setText(frontend.list.js.name_id, frontend.list.js.name)
    setText(frontend.list.js.number_id, frontend.list.js.number)
    setText(frontend.list.ts.name_id, frontend.list.ts.name)
    setText(frontend.list.ts.number_id, frontend.list.ts.number)
    setText(frontend.list.react.name_id, frontend.list.react.name)
    setText(frontend.list.react.number_id, frontend.list.react.number)
    setText(frontend.list.angular.name_id, frontend.list.angular.name)
    setText(frontend.list.angular.number_id, frontend.list.angular.number)
    const backend = data.tabs.skills.content.backend
    setText(backend.title_id, backend.title)
    setText(backend.subtitle_id, backend.subtitle)
    const design = data.tabs.skills.content.design
    setText(design.title_id, design.title)
    setText(design.subtitle_id, design.subtitle)
    /* Qualification */
    const qualification = data.tabs.skills.qualification
    setText(qualification.title_id, qualification.title)
    setText(qualification.subtitle_id, qualification.subtitle)


    setText(data.tabs.services.tab_title_id, data.tabs.services.tab_title)

    setText(data.tabs.portfolio.tab_title_id, data.tabs.portfolio.tab_title)

    setText(data.tabs.contactme.tab_title_id, data.tabs.contactme.tab_title)


}).catch(err => {
    // Do something for an error here
});


function setText(id, text) {
    const element = document.getElementById(id)
    if (element) {
        element.innerHTML = text;
    }
}



/* fetch('https://api.coinmarketcap.com/v2/ticker/1312/')
    .then(res => res.json())
    .then((res) => {
        const data = res.data;
        getElement('name').innerHTML = 'Name: ' + data.name;
        getElement('symbol').innerHTML = 'Symbol: ' + data.symbol;
        getElement('rank').innerHTML = 'Rank: ' + data.rank;
        getElement('price').innerHTML = 'Price: ' + data.quotes.USD.price;
        // do the rest here
    }); */